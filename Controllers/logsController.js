const fs = require('fs');
const LOGS_PATH = './logs.json';
const ENCODING = 'utf8';

function writeFile(content, errorHandler, callback) {
    fs.writeFile(LOGS_PATH, JSON.stringify(content, null, 4), ENCODING, err => { //saving logs
        if (err) {
            errorHandler(err, 500);
        }
        callback();
    });
}

function writeLogs(logsFile, message, errorHandler, callback) {
    logsFile['logs'].push({     //add new log
        message,
        time: Date.now()
    });
    
    writeFile(logsFile, errorHandler, callback);
}

const logsController = {
    log: function(message, errorHandler, callback) {
    
        let logsFile = {'logs': []};

        if ( fs.existsSync('./logs.json')) { //check for existence and correctness
            fs.readFile('./logs.json', 'utf8', (err, content) => {  //loading logs
                if (err) {
                    errorHandler(err, 500);
                }
                if ( content && JSON.parse(content)['logs']) {
                    logsFile = JSON.parse(content);
                }
                writeLogs(logsFile, message, errorHandler, callback);
            });
        } else {
            writeLogs(logsFile, message, errorHandler, callback);
        }
        
    }
}

module.exports = logsController;