const fs = require('fs');
const url = require('url');
const logsController = require('./Controllers/logsController');

function getErrorHandler (response) {
    return function(message, status) {
        response.writeHead(status, {'Content-type': 'text/html'});
        response.end(message);
    }
}

let router = {
    getHandler: function(request, response) {

        const { pathname, query } = url.parse(request.url, true);
        const errorHandler = getErrorHandler(response);
        
        switch (pathname) {

            case '/logs':

                fs.readFile('./logs.json', (err, content) => {
                    if (err) {
                        errorHandler('Cannot open logs.', 500);
                        return;
                    }
                    if (query.from && query.to) {
                        content = JSON.parse(content);
                        content['logs'] = content['logs'].filter(log => log['time'] >= query.from && log['time'] <= query.to);
                        content = JSON.stringify(content, null, 4);
                    }
    
                    logsController.log('Reading logs.', errorHandler, () => {
                        response.writeHead(200, {'Content-type': 'application/json'});
                        response.end(content);
                    });
                    
                });
                break;
        
            default:
                let filename = pathname.replace('/file/', '');
                fs.readFile(`./files/${filename}`, (err, content) => {
                    if (err) {
                        errorHandler(`No file with name "${filename}" found`, 400);
                        return;
                    }

                    logsController.log(`Reading file ${filename}.`, errorHandler, () => {
                        response.writeHead(200, {'Content-type': 'text/html'});
                        response.end(content);
                    });
                });
                break;
        }

    },
    postHandler: function(request, response) {

        const { pathname, query } = url.parse(request.url, true);
        const errorHandler = getErrorHandler(response);

        if ( !fs.existsSync('./files')) {
            fs.mkdir('./files', (err) => {
                if (err) {
                    errorHandler(err, 500);
                }
            });
        }
        if ( pathname === '/file' && query.filename  && query.content ) {
            fs.writeFile('files/' + query.filename, query.content.replace(new RegExp('_', 'g'),' '), 'utf8', err => {  //replace underscore with space
                if (err) {
                    errorHandler(`File ${query.filename} writing error.`, 500);
                }
            });

            logsController.log(`New file with name '${query.filename}' saved.`, errorHandler, () => {
                response.writeHead(200, {'Content-type': 'text/html'});
                response.end('File successfully created.');
            });
            
        } else {
            errorHandler('POST method can be accessed: localhost:8080/file?filename=:filename&content=:content', 400);
            return;
        }

    }
};
module.exports = router;