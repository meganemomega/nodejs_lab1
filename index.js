const http = require('http');
const router = require('./router');

module.exports = () => {
    http.createServer(function(request, response) {

        switch (request.method) {
            case 'POST':
                router.postHandler(request, response);
                break;
            default:
                router.getHandler(request, response);
                break;
        }

    }).listen(process.env.PORT || 8080);
}

